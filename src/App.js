// eslint-disable-next-line import/no-unresolved
import React from 'react';
import Images from './components/Images';
function App() {
  return (
    <div>
      <h3 className='heading'>Image Slider</h3>
      <Images />
    </div>
  );
}

export default App;
