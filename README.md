<h1 align="center">
  <br>
  Image Slider
  <br>
</h1>

<p align="center">
  <a href="#">
    <img src="https://badge.fury.io/js/electron-markdownify.svg"
         alt="Gitter">
  </a>
</p>

<p align="center">
  • <a href="#how-to-use">How To Use</a> •
  <a href="#built-with">Built With</a> •
  <a href="#use-case">Use Case</a> •
  <a href="#file-structure">File Stucture</a> •

</p>

## Use Case

> This Application build by using react-create-app. Only react native library has been used in order to create this application. Image slider could scale and added more images link to the image data file. In order to see next image swap, please!

## Built With

 <h2>React  <a href="https://opencollective.com/choo/sponsor/0/website" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/320px-React-icon.svg.png" height="100px" style="float:right"></a> </h2>
 <hr>

## Tools

- npm
- React
- Sass
- Visual Studio Code
- webpack
- Git


## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com) and [npm](https://nodejs.org/en/) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone https://github.com/Bekhzod96/image-slider.git

# Go into the repository
$ cd image-slider

# To install all dependency
$ npm i

# To install all dependency
$ npm start

```

## Authors

👤 **Bekhzod Akhrorov**

- Github:[@Bekhzod96](https://github.com/Bekhzod96)
- Twitter: [@Begzod](https://twitter.com/25d47e8987f740b)
- Linkedin:[@Bekhzod AKhrorov](https://www.linkedin.com/in/bekhzod-akhrorov/)
